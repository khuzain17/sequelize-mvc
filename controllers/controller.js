const {
    users
} = require('../models')

class Controller {
    static showUsers(req, res) {
        const usersArr = []
        users.findAll({})
            .then(Users => {
                for (const user of Users) {
                    usersArr.push({
                        email: user.dataValues.email,
                        fName: user.dataValues.first_name,
                        lName: user.dataValues.last_name,
                    })
                }
                res.render('show', {
                    usersArr
                })
            })
    }
    static upload(req, res) {
        console.log(req.body)
        users.create ({
            email: req.body.email,
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            password: req.body.password
        }) .then(users => {
            res.send(users)  
        })
    }

    static cekuser(req, res) {
        const email = req.body.email
        const password = req.body.password
        console.log("email user =  " + email)
        console.log("password user =  " + password)
    
        users.findAll({
            where: {
                email: email
            }
        }) .then(user => {
            res.send(user)  
        }) 
    }
}

module.exports = Controller