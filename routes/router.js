const express = require('express')
const router = express.Router()
const { users } = require('../models')

const controller = require('../controllers/controller')

router.get('/showUsers', controller.showUsers)
router.post('/upload', controller.upload)
router.post('/cekuser', controller.cekuser)

module.exports = router