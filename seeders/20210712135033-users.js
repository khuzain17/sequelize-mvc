'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [{
      email: "michael.lawson@reqres.in",
      password: "11223312",
      first_name: "Michael",
      last_name: "Lawson",
      city: "Germany",
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      email: "lindsay.ferguson@reqres.in",
      password: "password",
      first_name: "Lindsay",
      last_name: "Ferguson",
      city: "Mexico",
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      email: "tobias.funke@reqres.in",
      password: "foreigrns",
      first_name: "Tobias",
      last_name: "Funke",
      city: "Mexico",
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      email: "byron.fields@reqres.in",
      password: "bernacles",
      first_name: "Byron",
      last_name: "Fields",
      city: "UK",
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      email: "george.edwards@reqres.in",
      password: "indonesian",
      first_name: "George",
      last_name: "Edwards",
      city: "USA",
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      email: "edain.fey@reqres.in",
      password: "malaysia",
      first_name: "Edain",
      last_name: "fey",
      city: "France",
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      email: "isabell.nory@reqres.in",
      password: "qwerty",
      first_name: "Isabell",
      last_name: "Nory",
      city: "Indonesia",
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      email: "gita.ginanjar@reqres.in",
      password: "helloworld",
      first_name: "Gita",
      last_name: "Ginanjar",
      city: "Sweden",
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      email: "sorry.layhawa@reqres.in",
      password: "yukkeepsmile",
      first_name: "Enduas",
      last_name: "Mahpud",
      city: "Pinoy",
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      email: "one.direction@reqres.in",
      password: "sudahbubar",
      first_name: "Myas",
      last_name: "Werua",
      city: "Deutch",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};